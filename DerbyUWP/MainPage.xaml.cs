﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace DerbyUWP {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {
        DispatcherTimer _dispatcherTimer;
        SolidColorBrush green = new SolidColorBrush(Colors.Green);
        SolidColorBrush red = new SolidColorBrush(Colors.Red);

        public MainPage() {
            InitializeComponent();
            //numberOfLanes.SelectionChanged += new SelectionChangedEventHandler(OnNumberOfLanesChanged);
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 150);
            _dispatcherTimer.Tick += UpdateUI;
            _dispatcherTimer.Start();

            resetButton.Click += ResetButton_Click;
        }

        //private void OnNumberOfLanesChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (numberOfLanes == null) return;
        //    var combo = (ComboBox) sender;
        //    var item = (ComboBoxItem) combo.SelectedItem;
        //    Debug.WriteLine( "The selected item is " + item.Content.ToString());
        //    Watcher.numberOfLanes = Int32.Parse(item.Content.ToString());
        //}

        private void UpdateUI(object sender, object e) {
            if(Watcher.gpio == null) {
                Debug.WriteLine("GPIO not present");
                return;
            }

            // Check the gate
            if (Watcher.gateUp) {
                Debug.WriteLine("Gate up (switch closed)");
                gateStatus.Text = "Gate is Up";
            } else {
                Debug.WriteLine("Gate down (switch open)");
                gateStatus.Text = "Gate is Down";
            }

            // Check the lanes
            if (Watcher.ready[0] == true) {
                lane1Title.Foreground = green;
                Debug.WriteLine("Lane 1 - Ready");
            } else {
                lane1Title.Foreground = red;
                Debug.WriteLine("Lane 1 - Not Ready");
            }
        
            if (Watcher.ready[1] == true) {
                lane2Title.Foreground = green;
                Debug.WriteLine("Lane 2 - Ready");
            } else {
                lane2Title.Foreground = red;
                Debug.WriteLine("Lane 2 - Not Ready");
            }

            if (Watcher.ready[2] == true) {
                lane3Title.Foreground = green;
                Debug.WriteLine("Lane 3 - Ready");
            } else {
                lane3Title.Foreground = red;
                Debug.WriteLine("Lane 3 - Not Ready");
            }

            // Show the times
            if (Watcher.untripped[0] == false) {
                TimeSpan ts = TimeSpan.FromMilliseconds(Watcher.times[0]);
                lane1Status.Text = ts.ToString(@"%m\:ss\.fff");
            }
            if (Watcher.untripped[1] == false)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(Watcher.times[1]);
                lane2Status.Text = ts.ToString(@"%m\:ss\.fff");
            }
            if (Watcher.untripped[2] == false)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(Watcher.times[2]);
                lane3Status.Text = ts.ToString(@"%m\:ss\.fff");
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e) {
            Watcher.Reset();
            lane1Status.Text = "0:00.000";
            lane2Status.Text = "0:00.000";
            lane3Status.Text = "0:00.000";
        }
    }
}
