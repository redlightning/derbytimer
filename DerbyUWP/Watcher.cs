﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using System.Threading;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Storage;
using Windows.System.Threading;

namespace DerbyUWP {
    /// <summary>
    /// Polls the GPIO pins to check when the race has started, and when the cars have crossed the finish line
    /// </summary>
    sealed public class Watcher {
        public static GpioController gpio;
        private static GpioPin lane1, lane2, lane3, gatePin;
        public static Stopwatch stopwatch;
        public static bool running;
        public static int numberOfLanes = 3;
        public static long[] times = new long[] { 0, 0, 0, 0, 0, 0 };
        public static bool[] untripped = new bool[] { true, true, true, true, true, true };
        public static bool[] ready = new bool[] { true, true, true, true, true, true, true };
        public static bool gateUp = true;

        /// <summary>
        /// Set up the GPIO pins
        /// </summary>
        public Watcher() {
            // Get the default GPIO controller on the system
            gpio = GpioController.GetDefault();
            if (gpio == null)
                return; // GPIO not available on this system

            lane1 = gpio.OpenPin(17);
            lane1.SetDriveMode(GpioPinDriveMode.Input);
            lane2 = gpio.OpenPin(27);
            lane2.SetDriveMode(GpioPinDriveMode.Input);
            lane3 = gpio.OpenPin(22);
            lane3.SetDriveMode(GpioPinDriveMode.Input);;
            gatePin = gpio.OpenPin(18);
            gatePin.SetDriveMode(GpioPinDriveMode.Input);

            stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Check the gate, and then if it's down the lanes to capture when the cars cross the finish line.
        /// </summary>
        public static void Run() {
            //Check the gate - When up it will be pulled to ground (low)
            if (gatePin.Read() == GpioPinValue.High && !running) {
                Debug.WriteLine("And we're off!");
                stopwatch.Reset();
                stopwatch.Start();
                running = true;
                gateUp = false;
            } else if (gatePin.Read() == GpioPinValue.Low && running) {
                running = false;
                gateUp = true;
            } else if (gatePin.Read() == GpioPinValue.Low && !running) {
                CheckSensors();
                gateUp = true;
            }

            //Check the lanes
            if (running) {
                if (untripped[0] && lane1.Read() == GpioPinValue.High) {
                    times[0] = stopwatch.ElapsedMilliseconds;
                    untripped[0] = false;
                }
                if (untripped[1] && lane2.Read() == GpioPinValue.High) {
                    times[1] = stopwatch.ElapsedMilliseconds;
                    untripped[1] = false;
                }
                if (untripped[2] && lane3.Read() == GpioPinValue.High) {
                    times[2] = stopwatch.ElapsedMilliseconds;
                    untripped[2] = false;
                }
            }
            Task.Delay(TimeSpan.FromMilliseconds(50));
        }

        /// <summary>
        /// Reset for the next race
        /// </summary>
        public static bool Reset() {
            running = false;
            times = new long[] { 0, 0, 0, 0, 0, 0 };
            ready = new bool[] { true, true, true, true, true, true };
            untripped = new bool[] { true, true, true, true, true, true };
            CheckSensors();
            return true;
        }

        /// <summary>
        /// Check the status of the LEDs to make sure they are all reading correctly
        /// </summary>
        public static void CheckSensors() {
            // Gate - When closed will go to ground.
            if (gatePin.Read() == GpioPinValue.Low) {
                Debug.WriteLine("Gate - Up (Switch closed)");
                gateUp = true;
            } else {
                Debug.WriteLine("Gate - Down (Switch open)");
                gateUp = false;
            }

            // Lanes
            if (lane1.Read() == GpioPinValue.High) {
                Debug.WriteLine("Lane 1 - Not receiving singnal");
                ready[0] = false;
            } else {
                Debug.WriteLine("Lane 1 - OK");
                ready[0] = true;
            }

            if (lane2.Read() == GpioPinValue.High) {
                Debug.WriteLine("Lane 2 - Not receiving singnal");
                ready[1] = false;
            } else {
                Debug.WriteLine("Lane 2 - OK");
                ready[1] = true;
            }

            if (lane3.Read() == GpioPinValue.High && numberOfLanes >= 3) {
                Debug.WriteLine("Lane 3 - Not receiving singnal");
                ready[2] = false;
            } else {
                Debug.WriteLine("Lane 3 - OK");
                ready[2] = true;
            }
        }
    }
}